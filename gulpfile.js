// Gulp plugins die geladen werden.
var gulp            = require('gulp');
var ViewportTheme   = require('gulp-viewport');

// Target spricht die .viewportrc an(in diesem Fall LOC für Lokal)
var target          = 'LOC';
// themeName ist die Bezeichnung des Themes und muss identisch wie in Viewport selbst sein.
var themeName       = 'YourThemeName';

// wir kreieren das Object viewportTheme mit unseren Variablen
var viewportTheme   = new ViewportTheme({
    env:        target,
    themeName:  themeName,
    // Dieser Ordner wird ignoriert und alle files die darin sich befinden, in der Hierarchie "hochgezogen"
    sourceBase: 'src'
});


// Task um alle .vm Dateien zu finden und hochzuladen in unser viewport-theme, danach ablage in Ordner 'build'
gulp.task('templates', function() {
    return gulp.src('src/**/*.vm')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build'));
});

// Task um die Datei style.css hochzuladen in unser viewport-theme, danach ablage in Ordner 'build'
gulp.task('css', function () {
    return gulp.src('src/css/style.css')
        .pipe(viewportTheme.upload())
        .pipe(gulp.dest('build'))
});

// Task um alle Dateien in unserem viewport-theme zu löschen.
gulp.task('resetTheme', function() {
    viewportTheme.removeAllResources();
});

// Task um die tasks "templates" und "css" auszuführen
gulp.task('upload', ['templates', 'css']);

// Watch um die Dateien mit .vm und alle Datein im Ordner CSS zu überwachen. Wenn diese sich ändern, dann den entsprechenden task "templates" oder "CSS" ausführen.
gulp.task('watch', function () {
    gulp.watch('src/css/*.*', ['css']);
    gulp.watch('src/**/*.vm', ['templates']);
});